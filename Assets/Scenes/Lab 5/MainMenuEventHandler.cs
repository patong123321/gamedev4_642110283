using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class MainMenuEventHandler : MonoBehaviour
{
    private UIDocument _uiDocument;
    
    private VisualElement _Play;
            
    // Start is called before the first frame update
    void Awake()
    {
        _uiDocument = FindObjectOfType<UIDocument>();
        _Play = _uiDocument.rootVisualElement.Query<Button>("Play");
    }
    private void OnEnable()
    {
        _Play.RegisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
    }
    
    private void OnStartButtonMouseDownEvent(ClickEvent evt)
    {
        SceneManager.LoadSceneAsync("HUD");
    }
    
    private void OnDisable()
    {
        _Play.UnregisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
    }
}
