using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class BackEventHandler : MonoBehaviour
{
    private UIDocument _uiDocument;
    
    private VisualElement _Back;
            
    // Start is called before the first frame update
    void Awake()
    {
        _uiDocument = FindObjectOfType<UIDocument>();
        _Back = _uiDocument.rootVisualElement.Query<Button>("Back");
    }
    private void OnEnable()
    {
        _Back.RegisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
    }
    
    private void OnStartButtonMouseDownEvent(ClickEvent evt)
    {
        SceneManager.LoadSceneAsync("Title");
    }
    
    private void OnDisable()
    {
        _Back.UnregisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
    }
}
